<?php
/**
 * Created by PhpStorm.
 * User: agaizauskas
 * Date: 14/03/2017
 * Time: 15:38
 */

namespace AgeCalculator;


use AgeCalculator\Entity\Age;
use AgeCalculator\Formatter\AgeFormatter;

class Calculator
{
    /**
     * @var \DateTimeZone
     */
    protected $tz = null;

    /**
     * @var \AgeCalculator\Entity\Age
     */
    protected $ageEntity = null;

    /**
     * @var \AgeCalculator\Formatter\AgeFormatter
     */
    protected $formatter = null;

    /**
     * Error container
     *
     * @var array
     */
    protected $errors = array();

    /**
     * AgeCalculator constructor.
     *
     * @param string $timezoneName
     *
     * @throws \Exception
     */
    public function __construct($timezoneName = "Europe/London")
    {
        try{
            $this->instantiateTimeZone($timezoneName);
            $this->instantiateAgeEntity('\AgeCalculator\Entity\Age');
            $this->instantiateAgeFormatter('\AgeCalculator\Formatter\AgeFormatter');
        } catch (\Exception $e) {
            $this->addError($e->getMessage());
        }
    }

    /**
     * Initialize age entity
     *
     * @param $entityName
     */
    protected function instantiateAgeEntity($entityName)
    {
        $this->ageEntity = new $entityName();
    }

    /**
     * Initialize formatter
     *
     * @param $formatterName
     */
    protected function instantiateAgeFormatter($formatterName)
    {
        $this->formatter = new $formatterName();
    }

    /**
     * Initialize timezone format from string
     *
     * @param $timezoneName
     */
    protected function instantiateTimeZone($timezoneName)
    {
        $this->tz = new \DateTimeZone($timezoneName);
    }

    /**
     * Calculate difference between 2 dates in \DateTime format
     *
     * @param \DateTime $now
     * @param \DateTime $target
     *
     * @return \AgeCalculator\Entity\Age
     */
    protected function calculate(\DateTime $now, \DateTime $target)
    {
        $age = clone $this->ageEntity;

        $diff = $now->diff($target);
        $age->setAge($diff);
        $age->setYears($diff->y);
        $age->setMonths($age->getYears() * 12 + $diff->m);
        $age->setDays($diff->days);
        $age->setHours($age->getDays() * 24 + $diff->h);
        $age->setMinutes($age->getHours() * 60 + $diff->i);
        $age->setSeconds($age->getMinutes() * 60 + $diff->s);

        $tmd = clone $target;
        $tmd->modify("+".$age->getYears()." years");
        $tmDiff = $tmd->diff($target);
        $dtd = $age->getDays() - $tmDiff->days;
        $age->setDtd($dtd);

        return $age;
    }

    /**
     * Get current age based on provided birth date string and string format for \DateTime conversion
     *
     * @param        $birthDateString
     * @param string $inputFormat
     * @param string $outputFormat
     *
     * @return bool|string
     */
    public function getAge($birthDateString, $inputFormat = "d/m/Y H:i:s", $outputFormat = null)
    {
        if(! $this->formatter instanceof AgeFormatter) {
            $this->addError("Formatter class not instantiated");
            return false;
        }
        if(!$this->setFormat($outputFormat)){
            return false;
        }

        $birthDate = \DateTime::createFromFormat($inputFormat, $birthDateString, $this->getTz());
        $now = new \DateTime("now", $this->getTz());

        $age = $this->calculate($now, $birthDate);

        return $this->formatter->format($age);
    }

    /**
     * Set requested format rot age formatter
     *
     * @param string|null $format
     *
     * @return bool
     */
    public function setFormat($format)
    {
        if(is_null($format)){
            return true;
        }
        if(!in_array($format, $this->formatter->getAvailableFormats())){
            $this->addError("Incorrect format, please use \\AgeCalculator\\Formatter\\AgeFormatter class constants as available formats");
            return false;
        }
        $this->formatter->setFormat($format);

        return true;
    }

    /**
     * @return \DateTimeZone
     */
    public function getTz()
    {
        return $this->tz;
    }

    /**
     * @param \DateTimeZone $tz
     */
    public function setTz(\DateTimeZone $tz)
    {
        $this->tz = $tz;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     */
    protected function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @param $errorMessage
     */
    protected function addError($errorMessage)
    {
        $this->errors[] = $errorMessage;
    }

    /**
     * @return \AgeCalculator\Entity\Age
     */
    public function getAgeEntity()
    {
        return $this->ageEntity;
    }

    /**
     * @param \AgeCalculator\Entity\Age $ageEntity
     */
    public function setAgeEntity(Age $ageEntity)
    {
        $this->ageEntity = $ageEntity;
    }

    /**
     * @return \AgeCalculator\Formatter\AgeFormatter
     */
    public function getFormatter()
    {
        return $this->formatter;
    }

    /**
     * @param \AgeCalculator\Formatter\AgeFormatter $formatter
     */
    public function setFormatter(AgeFormatter $formatter)
    {
        $this->formatter = $formatter;
    }
}